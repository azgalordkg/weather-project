import React, {Component} from 'react';
import {connect} from "react-redux";
import WeatherTextInfo from "../../components/WeatherTextInfo/WeatherTextInfo";

import './App.css';
import {fetchWeatherData} from "../../store/actions/weatherActions";
import WeatherSearch from "../../components/WeatherSearch/WeatherSearch";
import WeatherMap from "../../components/WeatherMap/WeatherMap";

class App extends Component {
  state = {
    cityValue: '',
  };

  componentDidMount() {
    this.props.fetchWeatherData('Bishkek');
  }

  // Function for make two-way binding for search input

  changeHandler = event => {
    this.setState({cityValue: event.target.value});
  };

  // Function for Searching Weather in different cities

  searchWeather = event => {
    event.preventDefault();
    if (this.state.cityValue) {
      this.props.fetchWeatherData(this.state.cityValue);
    }
  };

  render() {
    if (!this.props.weatherList) {
      return <div>Loading...</div>
    }

    return (
      <div className="App">
        <WeatherSearch
          value={this.state.cityValue}
          change={this.changeHandler}
          searchClick={this.searchWeather}
        />
        <WeatherTextInfo
          city={this.props.city.name}
          list={this.props.weatherList}
        />
        <WeatherMap
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcA7WFDydKiMgV0Ppikxr__tGdZoWTNxk&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          key="AIzaSyBcA7WFDydKiMgV0Ppikxr__tGdZoWTNxk"
          lat={this.props.city.coord.lat}
          lng={this.props.city.coord.lon}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  weatherList: state.weather.list,
  city: state.weather.city,
});

const mapDispatchToProps = dispatch => ({
  fetchWeatherData: city => dispatch(fetchWeatherData(city)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
