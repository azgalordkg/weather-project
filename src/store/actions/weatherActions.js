import axios from '../../axios-weather';

export const GET_SUCCESS = 'GET_SUCCESS';
export const GET_FAILURE = 'GET_FAILURE';

export const getSuccess = response => ({type: GET_SUCCESS, response});
export const getFailure = error => ({type: GET_SUCCESS, error});

export const fetchWeatherData = city => {
  return dispatch => {
    axios.get(`/data/2.5/forecast/daily?q=${city}&units=metric&cnt=14&appid=88d326ce1ef3b65d2197d436055519ce`).then(
      response => {
        dispatch(getSuccess(response.data));
      },
      error => dispatch(getFailure(error)),
    );
  }
};