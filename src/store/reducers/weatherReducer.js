import {GET_FAILURE, GET_SUCCESS} from "../actions/weatherActions";

const initialState = {
    list: null,
    city: null,
    error: '',
};

const weatherReducer = (state = initialState, action) => {
    switch (action.type) {
        case (GET_SUCCESS):
            return {
                ...state,
                list: action.response.list,
                city: action.response.city,
            };
        case (GET_FAILURE):
            return {
                ...state,
                error: action.error,
            };
        default:
            return state;
    }
};

export default weatherReducer;