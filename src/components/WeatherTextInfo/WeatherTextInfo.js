import React from 'react';
import {week} from '../../constants';

import './WeatherTextInfo.css';
import WeatherChart from "../WeatherChart/WeatherChart";

const WeatherTextInfo = props => {
  return (
    <div className="WeatherTextInfo">
      <div uk-slider="true">
        <ul className="uk-slider-items uk-child-width-1-1">
          {props.list ? props.list.map((listItem, index) => {
            const date = new Date(listItem.dt * 1000);
            const currentMonth = date.getMonth();
            const currentDay = date.getDate();

            return (
              <li key={index}>
                <h2>City: <b>{props.city}</b></h2>
                <p>Date: {currentDay} {week[currentMonth]}</p>
                <div className="WeatherTextInfoTempMain">
                  <span>{Math.round(listItem.temp.day)} °C</span>
                  <img src={`http://openweathermap.org/img/w/${listItem.weather[0].icon}.png`} alt=""/>
                </div>
                <div className="WeatherTextInfoItem WeatherTextInfoMainData">
                  <p><b>Clouds:</b> {Math.round(listItem.clouds)}</p>
                  <p><b>Deg:</b> {Math.round(listItem.deg)}</p>
                  <p><b>Humidity:</b> {Math.round(listItem.humidity)} %</p>
                  <p><b>Pressure:</b> {Math.round(listItem.pressure)}</p>
                  <p><b>Speed:</b> {Math.round(listItem.speed)} m/s</p>
                </div>
                <WeatherChart
                  series={[
                    Math.round(listItem.temp.min),
                    Math.round(listItem.temp.morn),
                    Math.round(listItem.temp.night),
                    Math.round(listItem.temp.min),
                    Math.round(listItem.temp.max),
                  ]}
                />
              </li>
            );
          }) : null}
        </ul>
      </div>
    </div>
  );
};

export default WeatherTextInfo;