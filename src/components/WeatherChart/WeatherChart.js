import React, {Component} from 'react';
import ChartistGraph from 'react-chartist';

import './WeatherChart.css';

class WeatherChart extends Component {
  render() {
    const simpleLineChartData = {
      labels: ['Min', 'Morning', 'Evening', 'Night', 'Max'],
      series: [this.props.series,]
    };

    return (
      <div className="WeatherChart">
        <ChartistGraph data={simpleLineChartData} type={'Line'} />
      </div>
    );
  }
}

export default WeatherChart;