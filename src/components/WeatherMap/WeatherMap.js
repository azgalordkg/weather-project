import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import MapMarker from '../../assets/img/map-marker.png';
import {googleMapsStyles} from '../../googleMapsStyles';

import './WeatherMap.css';

const WeatherMap = withScriptjs(withGoogleMap((props) => {
  return (
    <div className="WeatherMap">
      <GoogleMap
        defaultZoom={12}
        center={{ lat: props.lat, lng: props.lng }}
        defaultCenter={{ lat: props.lat, lng: props.lng }}
        defaultOptions={{styles: googleMapsStyles}}
      >
        <Marker
          icon={MapMarker}
          position={{ lat: props.lat, lng: props.lng }}
        />
      </GoogleMap>
    </div>
  );
}));

export default WeatherMap;