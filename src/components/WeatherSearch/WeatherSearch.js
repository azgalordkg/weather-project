import React from 'react';

import './WeatherSearch.css';

const WeatherSearch = ({value, change, searchClick}) => {
  return (
    <div className="WeatherSearch">
      <form>
        <input
          value={value} onChange={change}
          type="text" className="WeatherSearchInput" placeholder="Search weather in City..."/>
        <button onClick={searchClick}>Search</button>
      </form>
    </div>
  );
};

export default WeatherSearch;