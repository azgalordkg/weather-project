import axios from 'axios/index';

const weather = axios.create({
  baseURL: 'http://api.openweathermap.org/',
});

export default weather;